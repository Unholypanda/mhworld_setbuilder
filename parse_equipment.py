import json

il = []
item_list = {
    'weapons': [],
    'charms': [],
    'helmets': [],
    'chests': [],
    'arms': [],
    'waists': [],
    'legs': []
    }

equipment_list = json.load(open('Equipment.json'))
for equipment in equipment_list:
    if 'Alpha' in equipment['Name'] or 'Beta' in equipment['Name']:
        item = {
            'id': equipment['ID'],
            'name': equipment['Name'],
            'type': equipment['Type'],
            'defense': equipment['Defense'],
            'fire_resist': equipment['Fire'],
            'water_resist': equipment['Water'],
            'thunder_resist': equipment['Thunder'],
            'ice_resist': equipment['Ice'],
            'dragon_resist': equipment['Dragon'],
            'rarity': equipment['Rarity'],
            'tier_1_slots': equipment['Slot_1'],
            'tier_2_slots': equipment['Slot_2'],
            'tier_3_slots': equipment['Slot_3'],
            'skills': []
            }
        il.append(item)

equipment_skill_list = json.load(open('EquipmentSkills.json'))
for skill in equipment_skill_list:
    for item in il:
        if item['id'] == skill['EID']:
            item_skill = {
                "skill_id": skill['SID'],
                "level": skill['Level']
                }
            item['skills'].append(item_skill)

for item in il:
    if item['type'] == '1':
        item['id'] = len(item_list['helmets'])
        item_list['helmets'].append(item)
    elif item['type'] == '2':
        item['id'] = len(item_list['chests'])
        item_list['chests'].append(item)
    elif item['type'] == '3':
        item['id'] = len(item_list['arms'])
        item_list['arms'].append(item)
    elif item['type'] == '4':
        item['id'] = len(item_list['waists'])
        item_list['waists'].append(item)
    else:
        item['id'] = len(item_list['legs'])
        item_list['legs'].append(item)
        
with open('result.json', 'w') as fp:
    json.dump(item_list, fp)
    
